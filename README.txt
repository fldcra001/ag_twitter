Program to simulate a twitter-like feed.

##############
Twitter-like feed
Craig Feldman
12 August 2015
##############

INSTRUCTIONS

Compile the program using the command: 'javac TwitterSimulator.java Tweet.java'
Run the program using the command: java TwitterSimulator [user file name] [tweets file name]
	e.g. 	java TwitterSimulator user.txt tweet.txt
		

NOTES

All output is sent to standard output.
Any errors are sent to standard error.

This program contains basic error checking such as ensuring that the tweets and user files are well formed.
Where possible, this program will try to continue processing the input files if an error occurs on a line.
This program will strip out whitespace in certain cases. e.g. if there are spaces in a twitter handle.

EXPECTED INPUT

This program accepts two files as command line arguments.

The first argument is the name of the file that contains the user input. The expected format of the file is as follows:

X follows A, B, C
Y follows X

The second argument is a list of tweets and is formatted as follows:

X> Hello World!
A> World says hello.

Where X, Y, A, B and C are user names.

TROUBLESHOOTING

Ensure that the input files are well formed and in the same directory as the class files.