/**
 * Simple class to associate a tweet to an author
 * This class is used so that tweets can be stored in time order.
 *
 * Created by Craig on 2015-08-11.
 */
public class Tweet {
	private String text;
	private String author;

	/**
	 * Constructor to generate a tweet.
	 * @param text the message content of the tweet
	 * @param author the author associated with the tweet
	 */
	public Tweet(String text, String author) {
		this.text = text;
		this.author = author;
	}

	/** @return the author associated with the tweet */
	public String getAuthor() {
		return author;
	}

	/** @return the message content of the tweet */
	public String getTweetContent() {
		return text;
	}
}
