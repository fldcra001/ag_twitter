/**
 * Allan Gray programming assignment.
 * 
 * Program to simulate a twitter-like feed.
 * 
 * @author Craig Feldman
 * 11 August 2015
 */
import java.io.FileReader;
import java.util.*;
import java.io.FileNotFoundException;

class TwitterSimulator {
	
	// Associates a user to a list of people they follow
	private static Map<String, Set<String>> users;

	// Stores a list of tweets that will be printed to console
	private static ArrayList<Tweet> tweets;

	public static void main(String args[]) {

		// Check that the required arguments are provided
		if (args.length != 2){
			System.err.println("Incorrect usage. Correct usage is: java TwitterSimulator [user file name] [tweets file name].");
			System.exit(1);
		}

		String userFileName = args[0];
		String tweetsFileName= args[1];

		users = new TreeMap<>();
		tweets = new ArrayList<>();

		processInput(userFileName, tweetsFileName);
		printTweets();
		
	}

	/** Prints the tweets to standard output */
	private static void printTweets() {
		for (String currentFeed: users.keySet()) {
			System.out.println(currentFeed + "\n");

			// Iterate through the tweets and output them appropriately
			for (Tweet t : tweets) {
				if (users.get(currentFeed).contains(t.getAuthor()))
					System.out.println("\t@" + t.getAuthor() + ": " + t.getTweetContent() +"\n");
			}
		}
	}

	/**
	 * Processes the input files.
	 *
	 * @param userFileName - The filename of the file that contains the user/following relationship.
	 * @param tweetsFileName - The filename of the file that contains the tweets.
	 */
	private static void processInput(String userFileName, String tweetsFileName){
		String line, currentUser;
		int lineNumber = 0;

		final String FOLLOW_KEYWORD = "follows";
		final String TWEET_START = "> ";

		// First read the user file
		try (Scanner in = new Scanner(new FileReader(userFileName))) {

			// Set that stores all the user names of people who the user follows
			HashSet<String> followsSet = null;

			while (in.hasNextLine()) {
				line = in.nextLine();
				++lineNumber;
				int keywordIndex = line.indexOf(FOLLOW_KEYWORD);
				// if follows keyword is not in line, there is an error in the line
				if (keywordIndex < 0){
					System.err.printf("Error processing line number %d of %s.%n", lineNumber, userFileName);
					// Skip to next line
					continue;
				}
				
				// get name of user for current line
				currentUser = line.substring(0, keywordIndex).trim();

				// process people this user follows
				String[] follows = line.substring(keywordIndex + FOLLOW_KEYWORD.length()).replaceAll("\\s+", "").split(",");
				// Create the set that will contain people this user follows
				followsSet = new HashSet<String>();
				for (String user: follows){
					followsSet.add(user);

					// Create this person if they don't exist yet
					if (!users.containsKey(user))
						users.put(user, new TreeSet<String>());
				}

				// This is done so that the tweets show not only to the followers, but also to the user who made the tweet.
				followsSet.add(currentUser);

				// associate the current user with the people they follow
				// if already in list, we must combine sets
				if (users.containsKey(currentUser))
					followsSet.addAll(users.get(currentUser));

				users.put(currentUser, followsSet);
			}

		/* DEBUG
			for (Map.Entry<String, Set<String>> entry : users.entrySet()) {
				String key = entry.getKey();
				Set<String> set = entry.getValue();
				System.out.println(key);
				for (String s : set)
					System.out.println("\t" + s);
			}
		*/

		} catch (FileNotFoundException e) {
			System.err.println("Error: User file (" + userFileName + ") not found - Check that the file exists");
			System.exit(1);
		}

		// Name of person who created the tweet.
		String author;

		// Now process the tweets file
		try (Scanner in = new Scanner(new FileReader(tweetsFileName))) {
			lineNumber = 0;
			while (in.hasNextLine()) {
				line = in.nextLine();
				++lineNumber;
				int tweetIndex = line.indexOf(TWEET_START);

				// if tweets keyword is not in line, there is an error in the line
				if (tweetIndex < 0){
					System.err.printf("Error processing line number %d of %s.%n", lineNumber, tweetsFileName);
					continue;
				}

				// get the name of the tweet author
				author = line.substring(0, tweetIndex).trim();
				// get the message.
				String tweet = line.substring(tweetIndex + 1).trim();

				if (tweet.length() > 140) {
					System.err.printf("Skipping tweet on line %d of %s. The tweet is too long (%d chars).%n", lineNumber, tweetsFileName, tweet.length());
					continue;
				}

				// Add the message and author to a list in order that they are created
				tweets.add(new Tweet(tweet, author));
			}

		/* DEBUG
			for (Tweet t : tweets) {
				System.out.println(t.getAuthor());
				System.out.println("\t" + t.getTweetContent());
			}
		*/

		} catch (FileNotFoundException e) {
			System.err.println("Error: Tweet file ("+ tweetsFileName +") not found - Check that the file exists");
			System.exit(1);
		}
	}
}
